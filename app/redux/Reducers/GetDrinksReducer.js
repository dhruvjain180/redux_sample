import { GET_DRINKS_FAIL, GET_DRINKS_START, GET_DRINKS_SUCCESS } from "../Actions/actionTypes";


const initialState = {
    drinksLoading : false,
    drinksSuccess: false,
    drinksData : null,
    drinksError: null
}
export default function GetDrinksReducer(state= initialState, action){
    switch(action.type){
        case GET_DRINKS_START:
            return {...state, drinksLoading: true}
        case GET_DRINKS_SUCCESS:
            return {...state, drinksLoading: false, drinksSuccess: true, drinksData: action.payload.data, drinksError: null}
        case GET_DRINKS_FAIL:
            return {...state, drinksLoading: false, drinksSuccess: false, drinksData: null, drinksError: action.payload.error}
        default:
            return state;
    }
}