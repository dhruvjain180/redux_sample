import * as actions from '../Actions/actionTypes';
const initalState  = {
    counter : 0
}

export function CounterReducer(state = initalState, action){
    switch(action.type){
        case actions.INCREASE_COUNTER:
            return {...state, counter: state.counter+1};
        default: 
            return state;
    }
}