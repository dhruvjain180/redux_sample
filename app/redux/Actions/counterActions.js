import { INCREASE_COUNTER } from "./actionTypes";

export const IncreaseAction = ()=>(
    {
        type: INCREASE_COUNTER,
    }
)