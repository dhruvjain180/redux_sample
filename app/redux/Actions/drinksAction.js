import axios from "axios"
import { GET_DRINKS_FAIL, GET_DRINKS_START, GET_DRINKS_SUCCESS } from "./actionTypes"

export const getDrinksAction = (page=1, per_page=10)=>(dispatch)=>{
    dispatch({type: GET_DRINKS_START})
    axios.get(`https://api.punkapi.com/v2//beers?page=${page}&per_page=${per_page}`)
    .then(res=>{
        dispatch({
            type: GET_DRINKS_SUCCESS,
            payload: {data:res}
        })
    })
    .catch(e=>{
        dispatch({
            type: GET_DRINKS_FAIL,
            payload: {error:e}
        })
    })
}