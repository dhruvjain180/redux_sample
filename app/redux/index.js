import { combineReducers } from "redux";
import { CounterReducer } from "./Reducers/CounterReducer";
import GetDrinksReducer from "./Reducers/GetDrinksReducer";

export const reducer = combineReducers({
    CounterReducer,
    GetDrinksReducer
})