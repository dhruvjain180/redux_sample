import * as React from 'react';
import { Text, View, StyleSheet, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { IncreaseAction } from '../redux/Actions/counterActions';
import { getDrinksAction } from '../redux/Actions/drinksAction';

interface CounterScreenProps {}

const CounterScreen = (props: CounterScreenProps) => {
  const counterReducer = useSelector(state=>state.CounterReducer);
  const dispatch = useDispatch();
  // const dispatch = useDispatch
  const drinksReducer = useSelector(state=>state.GetDrinksReducer);
  React.useEffect(()=>{
    dispatch(getDrinksAction())
  },[])

  React.useEffect(()=>{
    console.log('drinksReducer11', drinksReducer)
  },[drinksReducer])

  return (
    <View style={styles.container}>
      <Text>{counterReducer.counter}</Text>
      <TouchableOpacity style={styles.button} onPress={()=>{
        dispatch(IncreaseAction())
      }}>
        <Text>Increase</Text>
      </TouchableOpacity>
    </View>
  );
};

export default CounterScreen;

const styles = StyleSheet.create({
  container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center'
  }, 
  button:{
      marginTop: 20
  }
});

